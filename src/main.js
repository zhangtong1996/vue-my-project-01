import Vue from 'vue'
import App from './App.vue'

// 引入 router
import router from '@/router'

// 引入 element-UI
import ElementUI from 'element-ui'
import mbx from '@/plugins/mbx.vue'
import fy from '@/plugins/fy.vue'

// 引入 element-UI 的样式文件
import 'element-ui/lib/theme-chalk/index.css'
import './assets/css/global.css'
import axios from 'axios'
import VueQuillEditor from 'vue-quill-editor'
import TreeTable from 'vue-table-with-tree-grid'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
Vue.component('tree-table',TreeTable)
Vue.component(mbx.name, mbx)
Vue.component(fy.name, fy)

Vue.filter('dateFormat', function (time) { 
  const dt = new Date(time)
  const y = dt.getFullYear()
  const m = (dt.getMonth() + 1 + '').padStart(2, '0')
  const d = (dt.getDate()  + '').padStart(2, '0')
  const hh = (dt.getHours()+ '').padStart(2, '0')
  const mm = (dt.getMinutes() + '').padStart(2, '0')
  const ss = (dt.getSeconds() + '').padStart(2, '0')
  return `${y}-${m}-${d} ${hh}:${mm}:${ss}`
})
axios.interceptors.request.use(config => { 
  config.headers.Authorization = window.sessionStorage.getItem('token')
  
  
  return config
})
Vue.prototype.$http = axios
axios.defaults.baseURL=`http://127.0.0.1:8888/api/private/v1/`
Vue.config.productionTip = false

Vue.use(ElementUI)
Vue.use(VueQuillEditor)


new Vue({
  router, // 将路由挂载到根实例上
  render: h => h(App)
}).$mount('#app')